cmake_minimum_required(VERSION 2.8.3)
project(jointrider_arena)

## Add support for C++11, supported in ROS Kinetic and newer
add_definitions(-std=c++11)

# Load catkin and all dependencies required for this package
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  message_generation
  gazebo_ros
)

add_service_files(
  FILES
  SpawnButtonMarker.srv
)

generate_messages(
  DEPENDENCIES std_msgs geometry_msgs
)


catkin_package(
  CATKIN_DEPENDS
    roscpp
    gazebo_ros
    message_runtime
    std_msgs
)

# Depend on system install of Gazebo
find_package(gazebo REQUIRED)

link_directories(${GAZEBO_LIBRARY_DIRS})
include_directories(${Boost_INCLUDE_DIR} ${catkin_INCLUDE_DIRS} ${GAZEBO_INCLUDE_DIRS})
include_directories(${roscpp_INCLUDE_DIRS})
include_directories(${std_msgs_INCLUDE_DIRS})

# For world plugin
add_library(jointrider_world_plugin plugins/jointrider_world_plugin.cpp)
add_dependencies(jointrider_world_plugin ${catkin_EXPORTED_TARGETS})
target_link_libraries(jointrider_world_plugin ${roscpp_LIBRARIES} ${GAZEBO_LIBRARIES})
