#include "jointrider_world_plugin.hh"

using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(JointRiderWorldPlugin)

JointRiderWorldPlugin::JointRiderWorldPlugin() : WorldPlugin()
{
}

JointRiderWorldPlugin::~JointRiderWorldPlugin()
{
}

void JointRiderWorldPlugin::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
{
    this->world = _world;

    if (!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "jointrider_world_plugin_node",
                  ros::init_options::NoSigintHandler);
    }

    // Create our ROS node. This acts in a similar manner to
    // the Gazebo node
    this->rosNode.reset(new ros::NodeHandle());

    this->spawn_marker_service = this->rosNode->advertiseService<jointrider_arena::SpawnButtonMarker::Request,
                                                                jointrider_arena::SpawnButtonMarker::Response>("spawn_button_marker",
                                                                std::bind(&JointRiderWorldPlugin::SpawnButton, this,
                                                                std::placeholders::_1, std::placeholders::_2));

    // Connect to the world update event.
    this->worldConnection = event::Events::ConnectWorldUpdateBegin(
        std::bind(&JointRiderWorldPlugin::Spin, this));

    // Connect to the world update event.
    this->resetConnection = event::Events::ConnectWorldReset(
        std::bind(&JointRiderWorldPlugin::OnReset, this));
}

void JointRiderWorldPlugin::Spin()
{
    if (ros::ok())
    {
        ros::spinOnce();
    }
}

void JointRiderWorldPlugin::OnReset()
{
    for (std::vector<std::string>::iterator ni = this->marker_models.begin(); ni != this->marker_models.end(); ++ni)
    {
        this->world->RemoveModel(*ni);
    }
    
    this->marker_models.clear();

}

bool JointRiderWorldPlugin::SpawnButton(jointrider_arena::SpawnButtonMarker::Request &req, jointrider_arena::SpawnButtonMarker::Response &res)
{    
    std::string pose_x_str = std::to_string(req.pose_x);
    std::string pose_y_str = std::to_string(req.pose_y);
    std::string spawn_name = "marker_" + req.button_name;

    std::string sdf_file_string = "<?xml version='1.0' ?> "
                                  "<sdf version='1.5'>"
                                  "<include>"
                                  "<pose>" + pose_x_str + " " + pose_y_str + " 0 0 0 0" + "</pose>"
                                  "<name> " + spawn_name + "</name>"
                                  "<uri>model://marker</uri>"
                                  "</include>"
                                  "</sdf>";

    this->marker_models.push_back(spawn_name);
    this->world->InsertModelString(sdf_file_string);

}
