#ifndef GAZEBO_PLUGINS_JOINTRIDER_WORLD
#define GAZEBO_PLUGINS_JOINTRIDER_WORLD

#include "ros/ros.h"
#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include <gazebo/sensors/sensors.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

#include "jointrider_arena/SpawnButtonMarker.h"

namespace gazebo
{

    class GAZEBO_VISIBLE JointRiderWorldPlugin : public WorldPlugin
    {
    public:
        JointRiderWorldPlugin();
        virtual ~JointRiderWorldPlugin();
        virtual void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf);

    private:
        virtual void Spin();
        virtual void OnReset();
        virtual bool SpawnButton(jointrider_arena::SpawnButtonMarker::Request &req, jointrider_arena::SpawnButtonMarker::Response &res);

        std::unique_ptr<ros::NodeHandle> rosNode;
        ros::ServiceServer spawn_marker_service;
        physics::WorldPtr world;

        event::ConnectionPtr worldConnection, resetConnection;
        
        std::vector<std::string> marker_models;
    };
}
#endif